package cz.cvut.fel.ts1;

public class Puskapet {

    public static long factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException();
        }
        int result = 1;
        for (int i = n; i > 0; i--) {
            result = result * i;
        }
        return result;
    }
}