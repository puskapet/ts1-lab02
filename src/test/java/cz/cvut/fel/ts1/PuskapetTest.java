package cz.cvut.fel.ts1;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PuskapetTest {
    @Test
    public void factorialTest(){
        long actual = Puskapet.factorial(5);
        assertEquals(120, actual);
    }

    @Test
    public void factorialTest_zero() {
        long actual = Puskapet.factorial(0);
        assertEquals(1, actual);
    }

    @Test
    public void factorialTest_negativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> Puskapet.factorial(-1));
    }

}
